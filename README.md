# Composer Repository

This project holds all of our _private_ composer packages and builds our private packagist repository.

## Add a composer package/git repository

Open up `src/statis.json`.  Add your package here.  VCS stands for Version Control System.  Use VCS for repos in bitbucket/github.

## Testing/Staging

At some point we'll look to do automated testing, but for now you can test locally. Simply run the following:

```bash
$ composer run build && open build/index.html
```

## Deployment

This repo is configured for automatic builds and deployment. Pushes to `develop` will trigger a build. Pushes to `master` will build and deploy to [https://packages.ucdev.net/](https://packages.ucdev.net/).
